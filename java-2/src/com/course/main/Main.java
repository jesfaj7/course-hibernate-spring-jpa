package com.course.main;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.course.domain.DiarioCliente;
import com.course.service.DiarioClienteService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.course.domain.Tramite;
import com.course.service.TramiteService;

public class Main {


    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring_config.xml");

		Timestamp time = new Timestamp(new Date().getTime());
        TramiteService tramiteService = context.getBean(TramiteService.class);
        DiarioClienteService diarioClienteService = context.getBean(DiarioClienteService.class);
        Tramite tramite;
        List<Tramite> tramites;
        List<Tramite> results1;
        List<DiarioCliente> results2;

        tramite = new Tramite("Revision", time);
        tramiteService.save(tramite);
        System.out.println(tramiteService.findAll().toString());


        tramites = new ArrayList<>();
        tramite = new Tramite("Remodelación", time);
        tramites.add(tramite);
        tramite = new Tramite("Proyecto", time);
		tramites.add(tramite);
        tramiteService.save(tramites);
        System.out.println("Existe ID 1: " + tramiteService.exists(1));
        System.out.println("Existe ID 100: " + tramiteService.exists(100));
        System.out.print("\n\n");


        results1 = tramiteService.findByTipoTramLike("Proyecto");
        System.out.println("resultados(findByTipoTramLike): " + results1.size());
        System.out.println(results1.toString());
        System.out.print("\n\n");
		results1 = tramiteService.findByFhcTramLessThan("2018-12-21 21:45:16");
		System.out.println("resultados(findByFhcTramLessThan): " + results1.size());
		System.out.println(results1.toString());
        System.out.print("\n\n");

        tramiteService.deleteByTipoTram("Proyecto");

        results2 = diarioClienteService.findByTipoTram("Peritaje");
        System.out.println("results(findByTipoTram): " + results2.size());
        System.out.println(results2);
        System.out.print("\n\n");

        results2 = diarioClienteService.findByTipoTramAndFhcTram("Prestamo", "2018-12-21 22:00:00");
        System.out.println("results(findByTipoTramAndFhcTram): " + results2.size());
        System.out.println(results2);
        System.out.print("\n\n");

        ((ClassPathXmlApplicationContext)context).close();

    }

}