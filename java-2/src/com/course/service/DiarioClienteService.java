package com.course.service;

import com.course.domain.DiarioCliente;
import com.course.domain.Tramite;
import org.springframework.beans.factory.annotation.Autowired;
import com.course.repository.DiarioClienteRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DiarioClienteService {

    @Autowired
    private DiarioClienteRepository diarioClienteRepo;

    @Autowired
    private EntityManager entityManager;


    public List<DiarioCliente> findByTramite(Tramite tramite){
        return diarioClienteRepo.findByTramite(tramite);
    }

//    Usando QueryDSL
//    public List<DiarioCliente> findByTipoTram(String tipoTram){
//        QTramite tramite = QTramite.tramite;
//        QDiarioCliente diarioCliente = QDiarioCliente.diarioCliente;
//
//        return new JPAQuery(entityManager)
//                .from(tramite)
//                .join(tramite.diarioClienteSet, diarioCliente)
//                .where(tramite.tipoTram.eq(tipoTram))
//                .list(diarioCliente);
//    }

    public List<DiarioCliente> findByTipoTram(String tipoTram){
        return diarioClienteRepo.findByTramite_TipoTram(tipoTram);
    }

    public List<DiarioCliente> findByTipoTramAndFhcTram(String tipoTram, String fchTram){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date parsedDate = null;
        try{
            parsedDate = dateFormat.parse(fchTram);
        }
        catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        Timestamp timestamp = new Timestamp(parsedDate.getTime());

        return diarioClienteRepo.findByTramite_TipoTramAndTramite_FhcTramLessThan(tipoTram, timestamp);
    }
}
