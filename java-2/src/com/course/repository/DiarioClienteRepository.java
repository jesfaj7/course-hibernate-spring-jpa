package com.course.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.course.domain.DiarioCliente;
import com.course.domain.Tramite;

public interface DiarioClienteRepository extends CrudRepository<DiarioCliente, Integer>{

    List<DiarioCliente> findByTramite(Tramite tramite);

    List<DiarioCliente> findByTramite_TipoTram(String tipoTram);

    List<DiarioCliente> findByTramite_TipoTramAndTramite_FhcTramLessThan(String tipoTram, Timestamp fhcTram);

}