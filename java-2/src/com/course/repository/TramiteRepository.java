package com.course.repository;

import com.course.domain.Tramite;
import org.springframework.data.repository.CrudRepository;


import java.sql.Timestamp;
import java.util.List;

public interface TramiteRepository extends CrudRepository<Tramite, Integer> {
    public List<Tramite> findByTipoTramLike(String tipoTram);

    public List<Tramite> findByFhcTramLessThan(Timestamp date);

    public Integer deleteByTipoTram(String tipoTram);


}
