package com.course.main;

import java.sql.Timestamp;
import java.util.Date;

import com.course.dao.*;
import com.course.domain.*;

public class Main {

    public static void main(String[] args) {
        Timestamp time;
        Tramite tramite1;
        Tramite tramite2;
        Tramite tramite3;
        Avaluo avaluo1;
        DiarioCliente diarioCliente1;
        DiarioCliente diarioCliente2;
        DiarioCliente diarioCliente3;
        Inmueble inmueble1;
        Inmueble inmueble2;
        Inmueble inmueble3;
        Imagen imagen1;
        Imagen imagen2;
        Imagen imagen3;
        PersistenceDAO persistenceDAO;

        time = new Timestamp(new Date().getTime());
        tramite1 = new Tramite("Credito", time);
        tramite2 = new Tramite("Prestamo", time);
        tramite3 = new Tramite("Peritaje", time);
        avaluo1 = new Avaluo("Guatire, Miranda");
        diarioCliente1 = new DiarioCliente("1ra entrada", time);
        diarioCliente2 = new DiarioCliente("2da entrada", time);
        diarioCliente3 = new DiarioCliente("1ra entrada", time);

        inmueble1 = new Inmueble("Terreno" , "Ave. Primera");
        inmueble2 = new Inmueble("Edificio" , "Ave. Segunda");
        inmueble3 = new Inmueble("Edificio" , "Ave. Tercera");
        imagen1 = new Imagen("http://www.imgs.com/1" ,time);
        imagen2 = new Imagen("http://www.imgs.com/2" ,time);
        imagen3 = new Imagen("http://www.imgs.com/3" ,time);

        persistenceDAO = new PersistenceDAOImpl();
        persistenceDAO.insertAll(tramite1);//Insertamos un TRAMITE

        persistenceDAO.insertAll(tramite2);//Insertamos un TRAMITE(Para OneToOne)
        avaluo1.setTramite(tramite2);//Asignamos al avaluo un TRAMITE(Para OneToOne)
        persistenceDAO.insertAll(avaluo1);//Insertamos un AVALUO(Para OneToOne)

        persistenceDAO.insertAll(tramite3);//Insertamos un TRAMITE(Para OneToMany)
        diarioCliente1.setTramite(tramite3);//Asignamos al DIARIOCLIENTE un TRAMITE(Para ManyToOne)
        diarioCliente2.setTramite(tramite3);//Asignamos al DIARIOCLIENTE un TRAMITE(Para ManyToOne)
        diarioCliente3.setTramite(tramite2);//Asignamos al DIARIOCLIENTE un TRAMITE(Para ManyToOne)
        persistenceDAO.insertAll(diarioCliente1);//Insertamos un DIARIOCLIENTE(Para OneToMany)
        persistenceDAO.insertAll(diarioCliente2);//Insertamos un DIARIOCLIENTE(Para OneToMany)
        persistenceDAO.insertAll(diarioCliente3);//Insertamos un DIARIOCLIENTE(Para OneToMany)

        inmueble1.getImagenes().add(imagen1);
        inmueble1.getImagenes().add(imagen2);
        inmueble1.getImagenes().add(imagen3);
        inmueble2.getImagenes().add(imagen1);
        inmueble2.getImagenes().add(imagen2);
        inmueble3.getImagenes().add(imagen1);
        persistenceDAO.insertAll(inmueble1);
        persistenceDAO.insertAll(inmueble2);
        persistenceDAO.insertAll(inmueble3);

        //Consultas
        Tramite tramiteCons1;
        tramiteCons1 = new Tramite();
        tramiteCons1.setIdTram(1);
        System.out.println(">>By ID: \n" + persistenceDAO.findTramite(tramiteCons1, null, null) + "\n");
        tramiteCons1 = new Tramite();
        tramiteCons1.setTipoTram("Credito");
        System.out.println(">>By Tipo: \n" + persistenceDAO.findTramite(tramiteCons1, null, null) + "\n");
        tramiteCons1 = new Tramite();
        tramiteCons1.setFhcTram(time);
        System.out.println(">>By Fecha: \n" + persistenceDAO.findTramite(tramiteCons1, null, null) + "\n");

        Avaluo avaluoCons1;
        avaluoCons1 = new Avaluo();
        avaluoCons1.setIdAval(1);
        System.out.println(">>By ID: \n" + persistenceDAO.findTramite(null, avaluoCons1, null) + "\n");
        avaluoCons1 = new Avaluo();
        avaluoCons1.setLugarAval("Guatire, Miranda");
        System.out.println(">>By Lugar: \n" + persistenceDAO.findTramite(null, avaluoCons1, null) + "\n");

        DiarioCliente diarioCons1;
        diarioCons1 = new DiarioCliente();
        diarioCons1.setIdDiario(1);
        System.out.println(">>By ID: \n" + persistenceDAO.findTramite(null, null, diarioCons1) + "\n");
        diarioCons1 = new DiarioCliente();
        diarioCons1.setEntradaDiario("1ra entrada");
        System.out.println(">>By Tipo: \n" + persistenceDAO.findTramite(null, null, diarioCons1) + "\n");
        diarioCons1 = new DiarioCliente();
        diarioCons1.setFhcDiario(time);
        System.out.println(">>By Fecha: \n" + persistenceDAO.findTramite(null, null, diarioCons1) + "\n");
    }

}
