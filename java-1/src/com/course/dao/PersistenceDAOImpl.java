package com.course.dao;

import com.course.domain.*;
import com.course.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PersistenceDAOImpl implements PersistenceDAO {

    @Override
    public int insertAll(Object object) {
        //Activamos Hibernate
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        int status = 0;
        Tramite tramite;
        Avaluo avaluo;
        DiarioCliente diarioCliente;
        Inmueble inmueble;
        Imagen imagen;

        try {
            tx = session.beginTransaction();
            //Insertando
            if(object instanceof Tramite){
                tramite = (Tramite) object;
                session.save(tramite);
            }else if(object instanceof Avaluo){
                avaluo = (Avaluo) object;
                session.save(avaluo);
            }else if(object instanceof DiarioCliente){
                diarioCliente = (DiarioCliente) object;
                session.save(diarioCliente);
            }else if(object instanceof Inmueble){
                inmueble = (Inmueble) object;
                session.save(inmueble);
            }else if(object instanceof Imagen){
                imagen = (Imagen) object;
                session.save(imagen);
            }
            tx.commit();
            status = 1;
        }
        catch (Exception e) {
            if(tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }finally {
            //Cerramos
            session.close();
        }
        return status;
    }

    @Override
    public List<Tramite> findTramite(Tramite tramite, Avaluo avaluo, DiarioCliente diarioCliente) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Tramite> tramRes = null;
        List<Avaluo> avalRes = null;
        List<DiarioCliente> diarRes = null;
        try {
            if(tramite !=  null){//Consulting tramite
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<Tramite> criteriaTramite = builder.createQuery(Tramite.class);
                Root<Tramite> root = criteriaTramite.from(Tramite.class);

                criteriaTramite.where(builder.or(
                            builder.equal(root.get(Tramite_.idTram),tramite.getIdTram()),
                            builder.equal(root.get(Tramite_.tipoTram), tramite.getTipoTram()),
                            builder.equal(root.get(Tramite_.fhcTram), tramite.getFhcTram())
                        )
                );
                tramRes = session.createQuery(criteriaTramite).getResultList();
            }else if (avaluo !=  null){
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<Avaluo> criteriaAvaluo = builder.createQuery(Avaluo.class);
                Root<Avaluo> root = criteriaAvaluo.from(Avaluo.class);

                criteriaAvaluo.where(builder.or(
                        builder.equal(root.get(Avaluo_.idAval),avaluo.getIdAval()),
                        builder.equal(root.get(Avaluo_.lugarAval), avaluo.getLugarAval())
                        )
                );
                avalRes = session.createQuery(criteriaAvaluo).getResultList();
                if(avalRes != null){
                    tramRes = new ArrayList<>();
                    for (Avaluo itemAval : avalRes){
                        tramRes.add(itemAval.getTramite());
                    }
                }
            }else if (diarioCliente !=  null){
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<DiarioCliente> criteriaDiario = builder.createQuery(DiarioCliente.class);
                Root<DiarioCliente> root = criteriaDiario.from(DiarioCliente.class);

                criteriaDiario.where(builder.or(
                        builder.equal(root.get(DiarioCliente_.idDiario), diarioCliente.getIdDiario()),
                        builder.equal(root.get(DiarioCliente_.entradaDiario), diarioCliente.getEntradaDiario()),
                        builder.equal(root.get(DiarioCliente_.fhcDiario), diarioCliente.getFhcDiario())
                        )
                );
                diarRes = session.createQuery(criteriaDiario).getResultList();
                if(diarRes != null){
                    tramRes = new ArrayList<>();
                    for (DiarioCliente itemDiario : diarRes){
                        tramRes.add(itemDiario.getTramite());
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return tramRes;
    }

    @Override
    public Avaluo findAvaluo(Avaluo avaluo, Tramite tramite, DiarioCliente diarioCliente) {
        //TODO Buscar un avaluo usando el metodo findTramite
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Tramite> tramRes = null;
        List<Avaluo> avalRes = null;
        List<DiarioCliente> diarRes = null;
        try {
            if (avaluo !=  null){
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<Avaluo> criteriaAvaluo = builder.createQuery(Avaluo.class);
                Root<Avaluo> root = criteriaAvaluo.from(Avaluo.class);

                criteriaAvaluo.where(builder.or(
                        builder.equal(root.get(Avaluo_.idAval),avaluo.getIdAval()),
                        builder.equal(root.get(Avaluo_.lugarAval), avaluo.getLugarAval())
                        )
                );
                avalRes = session.createQuery(criteriaAvaluo).getResultList();
                if(avalRes != null){
                    tramRes = new ArrayList<>();
                    for (Avaluo itemAval : avalRes){
                        tramRes.add(itemAval.getTramite());
                    }
                }
            }else if(tramite !=  null){//Consulting tramite
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<Tramite> criteriaTramite = builder.createQuery(Tramite.class);
                Root<Tramite> root = criteriaTramite.from(Tramite.class);

                criteriaTramite.where(builder.or(
                        builder.equal(root.get(Tramite_.idTram),tramite.getIdTram()),
                        builder.equal(root.get(Tramite_.tipoTram), tramite.getTipoTram()),
                        builder.equal(root.get(Tramite_.fhcTram), tramite.getFhcTram())
                        )
                );
                tramRes = session.createQuery(criteriaTramite).getResultList();
            }else if (diarioCliente !=  null){
                CriteriaBuilder builder = session.getCriteriaBuilder();
                CriteriaQuery<DiarioCliente> criteriaDiario = builder.createQuery(DiarioCliente.class);
                Root<DiarioCliente> root = criteriaDiario.from(DiarioCliente.class);

                criteriaDiario.where(builder.or(
                        builder.equal(root.get(DiarioCliente_.idDiario), diarioCliente.getIdDiario()),
                        builder.equal(root.get(DiarioCliente_.entradaDiario), diarioCliente.getEntradaDiario()),
                        builder.equal(root.get(DiarioCliente_.fhcDiario), diarioCliente.getFhcDiario())
                        )
                );
                diarRes = session.createQuery(criteriaDiario).getResultList();
                if(diarRes != null){
                    tramRes = new ArrayList<>();
                    for (DiarioCliente itemDiario : diarRes){
                        tramRes.add(itemDiario.getTramite());
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return null;
    }

    @Override
    public DiarioCliente findDiarioCliente(DiarioCliente diarioCliente, Tramite tramite, Avaluo avaluo) {
        return null;
    }

    @Override
    public Inmueble findInmueble(Inmueble inmueble) {
        return null;
    }

    @Override
    public Imagen findImagen(Imagen imagen) {
        return null;
    }

    @Override
    public int updateTramite(Tramite tramite) {
        return 0;
    }

    @Override
    public int updateAvaluo(Avaluo avaluo) {
        return 0;
    }

    @Override
    public int updateDiarioCliente(DiarioCliente diarioCliente) {
        return 0;
    }

    @Override
    public int updateInmueble(Inmueble inmueble) {
        return 0;
    }

    @Override
    public int updateImagen(Imagen imagen) {
        return 0;
    }

    @Override
    public int deleteTramite(Tramite tramite) {
        return 0;
    }

    @Override
    public int deleteAvaluo(Avaluo avaluo) {
        return 0;
    }

    @Override
    public int deleteDiarioCliente(DiarioCliente diarioCliente) {
        return 0;
    }

    @Override
    public int deleteInmueble(Inmueble inmueble) {
        return 0;
    }

    @Override
    public int deleteImagen(Imagen imagen) {
        return 0;
    }

}
