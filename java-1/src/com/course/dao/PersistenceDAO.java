package com.course.dao;

import com.course.domain.*;
import java.util.List;

public interface PersistenceDAO {
    int insertAll(Object object);

    List<Tramite> findTramite(Tramite tramite, Avaluo avaluo, DiarioCliente diarioCliente);
    Avaluo findAvaluo(Avaluo avaluo, Tramite tramite, DiarioCliente diarioCliente);
    DiarioCliente findDiarioCliente(DiarioCliente diarioCliente, Tramite tramite, Avaluo avaluo);
    Inmueble findInmueble(Inmueble inmueble);
    Imagen findImagen(Imagen imagen);

    int updateTramite(Tramite tramite);
    int updateAvaluo(Avaluo avaluo);
    int updateDiarioCliente(DiarioCliente diarioCliente);
    int updateInmueble(Inmueble inmueble);
    int updateImagen(Imagen imagen);

    int deleteTramite(Tramite tramite);
    int deleteAvaluo(Avaluo avaluo);
    int deleteDiarioCliente(DiarioCliente diarioCliente);
    int deleteInmueble(Inmueble inmueble);
    int deleteImagen(Imagen imagen);
}
